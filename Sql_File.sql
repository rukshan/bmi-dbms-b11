CREATE DATABASE `csh_anc_database`;

USE `csh_anc_database`;

CREATE TABLE `doctors` (
  `slmc_id` INT UNIQUE NOT NULL,
  `first_name` VARCHAR(100) NOT NULL,
  `last_name` VARCHAR(100) NOT NULL,
  `designation` ENUM('ho', 'rho', 'mo', 'sho', 'reg', 'sr', 'vog') NOT NULL,
  PRIMARY KEY (`slmc_id`)
);

CREATE TABLE `phms` (
  `phm_id` INT UNIQUE NOT NULL,
  `first_name` VARCHAR(100),
  `last_name` VARCHAR(100),
  PRIMARY KEY(`phm_id`)
);

CREATE TABLE `patients` (
  `phn` VARCHAR(10) NOT NULL,
  `patient_name` TEXT NOT NULL,
  `el_register_date` DATE DEFAULT NULL,
  `el_register_number` VARCHAR(100) DEFAULT NULL,
  `rubella_vaccine` ENUM('yes', 'no') DEFAULT NULL,
  `date_of_birth` DATE NOT NULL,
  `consanguinity` ENUM('yes', 'no') DEFAULT NULL,
  `occupation` TEXT DEFAULT NULL,
  `education` ENUM('ol', 'al', 'degree', 'postgrad', 'phd', 'diploma') NOT NULL,
  `living_children` INT NOT NULL DEFAULT 0,
  `youngest_child_age` INT DEFAULT NULL,
  `height` INT NOT NULL,
  `weight` DECIMAL(6,3) NOT NULL,
  `nic` VARCHAR(20),
  `blood_group` ENUM('A', 'B', 'AB', 'O') NOT NULL,
  `rh_type` ENUM('positive', 'negative') NOT NULL,
  PRIMARY KEY (`phn`),
  UNIQUE KEY `nic` (`nic`)
);

CREATE TABLE `pregnancies` (
  `gravidity` INT NOT NULL,
  `phn` VARCHAR(10) NOT NULL,
  `last_contraceptive_method` TEXT DEFAULT NULL,
  `highest_education` ENUM('ol', 'al', 'degree', 'postgrad', 'phd', 'diploma') DEFAULT NULL,
  `occupation` TEXT DEFAULT NULL,
  `pregnancy_register_number` VARCHAR(100) DEFAULT NULL,
  `pregnancy_register_date` DATE DEFAULT NULL,
  `moh_area` VARCHAR(255) DEFAULT NULL,
  `pre_pregnancy_screening` ENUM('yes', 'no') DEFAULT NULL,
  `phm_area` VARCHAR(255) DEFAULT NULL,
  `age_on_pregnancy` INT NOT NULL,
  `date_of_quickening` DATE DEFAULT NULL,
  `poa_at_registration` INT NOT NULL,
  `lrmp` DATE NOT NULL,
  `uss_edd` DATE DEFAULT NULL,
  `age_of_youngest_child` INT DEFAULT NULL,
  `husband_education` ENUM('ol', 'al', 'diploma', 'degree', 'postgrad', 'phd') DEFAULT NULL,
  `husband_occupation` VARCHAR(100) DEFAULT NULL,
  `husband_dob` DATE DEFAULT NULL,
  `foleic_acid` ENUM('yes', 'no') DEFAULT NULL,
  `registered_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `height` INT NOT NULL,
  `weight` DECIMAL(6,3) NOT NULL,
  `at_risk` ENUM('yes', 'no') NOT NULL,
  `registered_by` INT NOT NULL,
  PRIMARY KEY (`gravidity`, `phn`),
  FOREIGN KEY (`phn`) REFERENCES `patients`(`phn`),
  FOREIGN KEY (`registered_by`) REFERENCES `phms`(`phm_id`)
);

CREATE TABLE `completed_pregnancies` (
  `gravidity` INT NOT NULL,
  `sex` ENUM ('male', 'female') NOT NULL,
  `outcome` ENUM('lb', 'sb', 'mc') NOT NULL,
  `place_of_delivery` VARCHAR(255) DEFAULT NULL,
  `mode_of_delivery` VARCHAR(100) NOT NULL,
  `antinatal_complication` TEXT DEFAULT NULL,
  `phn` VARCHAR(10) NOT NULL,
  `birth_weight` DECIMAL (6, 4) DEFAULT NULL,
  `postnatal_complications` TEXT DEFAULT NULL,
  `date_of_birth` DATE DEFAULT NULL,
  `date_of_miscarriage` DATE DEFAULT NULL,
  PRIMARY KEY (`phn`, `gravidity`),
  FOREIGN KEY (`phn`) REFERENCES `pregnancies`(`phn`)
);

CREATE TABLE `sessions` (
  `session_id` INT NOT NULL AUTO_INCREMENT,
  `poa` INT NOT NULL,
  `gravidity` INT NOT NULL,
  `phn` VARCHAR(10) NOT NULL,
  `session_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `slmc_id` INT NOT NULL,
  `clinic_number` VARCHAR(10),
  `appintment_number` VARCHAR(20),
  PRIMARY KEY (`session_id`),
  FOREIGN KEY (`gravidity`, `phn`) REFERENCES `pregnancies`(`gravidity`, `phn`),
  FOREIGN KEY (`slmc_id`) REFERENCES `doctors`(`slmc_id`)
);

CREATE TABLE `prescriptions` (
  `prescription_id` INT NOT NULL AUTO_INCREMENT,
  `issued_at` DATETIME DEFAULT CURRENT_TIMESTAMP,
  `session_id` INT NOT NULL,
  `phn` VARCHAR(10) NOT NULL,
  `gravidity` INT NOT NULL,
  `management` TEXT,
  `slmc_id` INT NOT NULL,
  PRIMARY KEY (`prescription_id`),
  FOREIGN KEY (`phn`, `gravidity`) REFERENCES `pregnancies`(`phn`, `gravidity`),
  FOREIGN KEY (`slmc_id`) REFERENCES `doctors`(`slmc_id`),
  FOREIGN KEY (`session_id`) REFERENCES `sessions`(`session_id`)
);


INSERT INTO `doctors`
  (`slmc_id`, `first_name`, `last_name`, `designation`)
VALUES
  (36213, 'Rukshan', 'Ranatunge', 'mo'),
  (36200, 'Sanjaya', 'Nagasinge', 'mo'),
  (34041, 'Rajayana', 'Jayarathne', 'vog'),
  (33452, 'Gayan', 'Kasthuriarachchi', 'reg'),
  (35321, 'Chamodi', 'Madhushani', 'mo'),
  (35645, 'Kalindi', 'Samarakoon', 'mo'),
  (28454, 'Kasun', 'Punsiri', 'VOG'),
  (25043, 'Charith', 'Fernando', 'rho'),
  (37541, 'Sanchana', 'Kariyawasam', 'rho'),
  (23543, 'Sulakshani', 'Morawaka', 'sho'),
  (33878, 'Sachin', 'Rachindra', 'reg'),
  (27458, 'Tharaka', 'Balasooriya', 'sr'),
  (33534, 'Chamara', 'Silva', 'mo'),
  (28994, 'Athula', 'Janaka', 'mo'),
  (24504, 'Gayan', 'Lakmal', 'mo'),
  (18934, 'Tiran', 'Dias', 'vog'),
  (25493, 'Rasika', 'Silva', 'vog'),
  (21293, 'Sanjeewa', 'Padumadasa', 'vog'),
  (21294, 'Chamika', 'Silva', 'reg'),
  (32304, 'Benoy', 'Ranatunge', 'mo');


INSERT INTO `phms`
  (`phm_id`, `first_name`, `last_name`)
VALUES
  (34294,'Kanthi','Silva'),
  (59402,'Kusum','Perera'),
  (45755,'Yamuna','Ranasinghe'),
  (54577,'Theja','Amarasinge'),
  (87987,'Kusum','Renu'),
  (57288,'Geetha','Kumarasinghe'),
  (79795,'Shanthi','Silva'),
  (77834,'Sunethra','Perera'),
  (40395,'Sithara','Pavirthra'),
  (33243,'Praveena','Perera'),
  (98565,'Nilushi','Perera'),
  (32423,'Shanthi','Amaraweera'),
  (23445,'Ganga','Amaraweera'),
  (87965,'Uthpala','Janaki'),
  (23414,'Apsara','Rathnayaka'),
  (43234,'Kumudu','Senevirathne'),
  (54452,'Charitha','Madawala'),
  (23423,'Madara','Ranasinghe'),
  (53442,'Shamila','Perera'),
  (32412,'Charhurika','Ranasinghe');

INSERT INTO `patients`
  (`phn`,
  `patient_name`,
  `el_register_date`,
  `el_register_number`,
  `rubella_vaccine`,
  `date_of_birth`,
  `consanguinity`,
  `occupation`,
  `education`,
  `living_children`,
  `youngest_child_age`,
  `height`,
  `weight`,
  `nic`,
  `blood_group`,
  `rh_type`)
VALUES
  ('30403', 'A Dinushika', '2020-10-12', 'B/241', 'yes', '1990-11-17', 'yes', NULL , 'al', 0, 0, 160, 60.8, '903223758V', 'A', 'positive'),
  ('30502', 'S Thilakarathne', '2019-01-11', 'A/121', 'yes', '1985-04-13', 'no', NULL , 'ol', 1, 10, 170, 75.0, '854523698V', 'B', 'negative'),
  ('60534', 'Nalika Sudarshani', '2017-05-10', 'N/403', 'no', '1994-02-17', 'no', NULL , 'al', 0, 0, 155, 68.8, '945369874V', 'AB', 'positive'),
  ('74512', 'Chamalika Ishadini', '2015-08-23', 'B/302', 'no', '1992-09-17', 'yes', 'Receptionist' , 'al', 0, 0, 160, 67.0, '925478965V', 'B', 'positive'),
  ('50643', 'Nisha Kyjenthini', '2017-12-26', 'C/2212', 'no', '1989-05-23', 'no', 'Lab assistant' , 'degree', 2, 7, 150, 64.0, '897456325V', 'B', 'positive'),
  ('65484', 'Sandya Nadushtika', '2016-07-06', 'F/4543', 'no', '1993-06-09', 'yes', 'Office clerk' , 'al', 1, 4, 155, 67.9, '901785412V', 'AB', 'negative'),
  ('63543', 'Udeshika Harshani', '2015-10-12', 'B/234', 'no', '1990-06-09', 'yes', NULL , 'al', 3, 6, 148, 58, '906547841V', 'O', 'positive'),
  ('34264', 'Chathuri Karunarathne', '2019-03-19', 'T/3456', 'no', '1990-06-05', 'yes', 'Office assistant' , 'postgrad', 2, 4, 178, 80, '905478563V', 'B', 'positive'),
  ('88453', 'Prabhavi Jayasiri', '2021-03-17', 'Y/421', 'yes', '1990-05-17', 'yes', 'Business analyst' , 'postgrad', 3, 2, 134, 55.4, '9057896321V', 'AB', 'negative'),
  ('55324', 'Sanduni Wihangika', '2017-11-12', 'U/324', 'no', '1985-11-17', 'yes', NULL , 'al', 2, 5, 167, 70, '859634785V', 'A', 'positive'),
  ('98241', 'Gaushala Wickramasinghe', '2018-11-12', 'F/998', 'no', '1991-12-17', 'yes', NULL , 'degree', 1, 4, 159, 56, '912378546V', 'O', 'positive'),
  ('59698', 'Kaushi Silva', '2019-10-29', 'F/764', 'no', '1997-03-01', 'no', NULL , 'degree', 0, 0, 164, 66, '978541236V', 'AB', 'positive'),
  ('87542', 'Jeromy Jayakumar', '2018-02-12', 'C/341', 'no', '1991-10-03', 'yes', 'Office assistant' , 'degree', 1, 3, 167, 72.3, '19912367852V', 'O', 'negative'),
  ('35789', 'Anuradha Darmathilaka', '2015-03-02', 'K/432', 'no', '1983-05-08', 'yes', 'Medical officer' , 'degree', 1, 4, 158, 76.0, '836547852V', 'AB', 'negative'),
  ('65248', 'Kithmini Perera', '2017-05-04', 'N/241', 'no', '1989-03-21', 'yes', 'Medical officer' , 'degree', 0, 0, 160, 76.0, '893657852V', 'A', 'positive'),
  ('45313', 'Nethmi Weerasinge', '2015-04-09', 'K/341', 'no', '1994-01-31', 'yes', NULL , 'al', 0, 0, 135, 65.3, '947852365V', 'B', 'positive'),
  ('47564', 'Pushpa Silva', '2014-09-25', 'U/321', 'no', '1984-10-17', 'yes', NULL , 'al', 3, 5, 147, 89.0, '19846587896V', 'AB', 'positive'),
  ('87451', 'Kusum Perera', '2012-06-23', 'R/235', 'no', '1985-01-21', 'yes', NULL , 'al', 2, 6, 178, 75.3, '857896354V', 'B', 'positive'),
  ('57985', 'Lochitha Nirmani Adikari', '2017-06-07', 'D/241', 'no', '1991-04-10', 'yes', 'Software engineer' , 'degree', 1, 4, 167, 65.4, '914785463V', 'A', 'negative'),
  ('86453', 'Y Dinushika', '2015-06-07', 'A/861', 'no', '1986-12-03', 'yes', 'Lawyer' , 'degree', 1, 6, 165, 65.3, '869874523V', 'AB', 'positive');

INSERT INTO `pregnancies`
  (`gravidity`, `phn`, `last_contraceptive_method`,
  `highest_education`, `occupation`, `pregnancy_register_number`,
  `pregnancy_register_date`, `moh_area`,
  `pre_pregnancy_screening`, `phm_area`, `age_on_pregnancy`,
  `date_of_quickening`, `poa_at_registration`, `lrmp`,
  `uss_edd`, `age_of_youngest_child`, `husband_education`,
  `husband_occupation`,
  `husband_dob`, `foleic_acid`, `height`, `weight`, `at_risk`,`registered_by`)
VALUES
  (
    1
    ,
    '30403',
    'ocp',
    (
      SELECT
        `education` FROM `patients`
      WHERE
        `phn` = '30403'
    ),
    (
      SELECT
        `occupation` FROM `patients`
      WHERE
        `phn` = '30403'
    ),
    '324',
    '2021-05-10',
    'Kantale',
    'no',
    'Kantale',
    (
      SELECT
        YEAR(CURRENT_TIMESTAMP) - YEAR(`date_of_birth`)
      FROM
        `patients`
      WHERE
        `phn` = '30403'
    ),
    '2021-06-30',
    4,
    '2021-04-05',
    '2022-03-15',
    (
      SELECT
        `youngest_child_age`
      FROM
        `patients`
      WHERE
        `phn` = '30403'),
    'al',
    'Businessman',
    '1985-04-05',
    'yes',
    (
      SELECT
        `height`
      FROM
        `patients`
      WHERE
        `phn` = '30403'
    ),
    (
      SELECT
        `weight`
      FROM
        `patients`
      WHERE
        `phn` = '30403'
    ),
    'no',
    34294
  ),
  (
    2
    ,
    '30502',
    'depo',
    (
      SELECT
        `education` FROM `patients`
      WHERE
        `phn` = '30502'
    ),
    (
      SELECT
        `occupation` FROM `patients`
      WHERE
        `phn` = '30502'
    ),
    '402',
    '2021-02-10',
    'Kegalle',
    'no',
    'Kegalle',
    (
      SELECT
        YEAR(CURRENT_TIMESTAMP) - YEAR(`date_of_birth`)
      FROM
        `patients`
      WHERE
        `phn` = '30502'
    ),
    '2021-03-12',
    3,
    '2021-01-15',
    '2021-11-15',
    (SELECT
      `youngest_child_age`
    FROM
      `patients`
    WHERE
      `phn` = '30502'),
    'ol',
    'Businessman',
    '1989-12-05',
    'yes',
    (
      SELECT
        `height`
      FROM
        `patients`
      WHERE
        `phn` = '30502'
    ),
    (
      SELECT
        `weight`
      FROM
        `patients`
      WHERE
        `phn` = '30502'
    ),
    'no',
    23445
  ),
  (
    1
    ,
    '60534',
    NULL,
    (
      SELECT
        `education` FROM `patients`
      WHERE
        `phn` = '60534'
    ),
    (
      SELECT
        `occupation` FROM `patients`
      WHERE
        `phn` = '60534'
    ),
    '4021',
    '2021-04-16',
    'Gampaha',
    'no',
    'Nittambuwa',
    (
      SELECT
        YEAR(CURRENT_TIMESTAMP) - YEAR(`date_of_birth`)
      FROM
        `patients`
      WHERE
        `phn` = '60534'
    ),
    '2021-05-15',
    5,
    '2022-03-15',
    '2022-01-20',
    (
      SELECT
        `youngest_child_age`
      FROM
        `patients`
      WHERE
        `phn` = '60534'
    ),
    'Degree',
    'Software Engineer',
    '1990-04-12',
    'yes',
    (
      SELECT
        `height`
      FROM
        `patients`
      WHERE
        `phn` = '60534'
    ),
    (
      SELECT
        `weight`
      FROM
        `patients`
      WHERE
        `phn` = '60534'
    ),
    'no',
    77834
  ),
  (
    1
    ,
    '74512',
    NULL,
    (
      SELECT
        `education` FROM `patients`
      WHERE
        `phn` = '74512'
    ),
    (
      SELECT
        `occupation` FROM `patients`
      WHERE
        `phn` = '74512'
    ),
    '658',
    '2021-06-01',
    'Gampaha',
    'no',
    'Ragama',
    (
      SELECT
        YEAR(CURRENT_TIMESTAMP) - YEAR(`date_of_birth`)
      FROM
        `patients`
      WHERE
        `phn` = '74512'
    ),
    NULL,
    8,
    '2021-04-25',
    '2022-02-10',
    (SELECT `youngest_child_age` FROM `patients` WHERE `phn` = '74512'),
    'Degree',
    'Civil Engineer',
    '1989-08-12',
    'yes',
    (
      SELECT
        `height`
      FROM
        `patients`
      WHERE
        `phn` = '74512'
    ),
    (
      SELECT
        `weight`
      FROM
        `patients`
      WHERE
        `phn` = '74512'
    ),
    'no',
    43234
  ),
  (
    3
    ,
    '50643',
    NULL,(
      SELECT
        `education` FROM `patients`
      WHERE
        `phn` = '50643'
    ),
    (
      SELECT
        `occupation` FROM `patients`
      WHERE
        `phn` = '50643'
    ),
    '9852',
    '2021-06-01',
    'Kandy',
    'no',
    'Kandy',
    (
      SELECT
        YEAR(CURRENT_TIMESTAMP) - YEAR(`date_of_birth`)
      FROM
        `patients`
      WHERE
        `phn` = '50643'
    ),
    '2021-05-12',
    7,
    '2021-02-05',
    '2022-01-12',
    (SELECT `youngest_child_age` FROM `patients` WHERE `phn` = '50643'),
    'al',
    'Mechanic',
    '1987-03-12',
    'yes',
    (
      SELECT
        `height`
      FROM
        `patients`
      WHERE
        `phn` = '50643'
    ),
    (
      SELECT
        `weight`
      FROM
        `patients`
      WHERE
        `phn` = '50643'
    ),
    'yes',
    57288
  ),
  (
    2
    ,
    '65484',
    NULL,(
      SELECT
        `education` FROM `patients`
      WHERE
        `phn` = '65484'
    ),
    (
      SELECT
        `occupation` FROM `patients`
      WHERE
        `phn` = '65484'
    ),
    '543',
    '2021-02-01',
    'Kurunagala',
    'no',
    'Kurunagala',
    (
      SELECT
        YEAR(CURRENT_TIMESTAMP) - YEAR(`date_of_birth`)
      FROM
        `patients`
      WHERE
        `phn` = '65484'
    ),
    '2021-03-12',
    6,
    '2020-12-20',
    '2021-10-15',
    (
      SELECT
        `youngest_child_age`
      FROM
        `patients`
      WHERE
        `phn` = '65484'
    ),
    'Degree',
    'Teacher',
    '1985-04-12',
    'yes',
    (
      SELECT
        `height`
      FROM
        `patients`
      WHERE
        `phn` = '65484'
    ),
    (
      SELECT
        `weight`
      FROM
        `patients`
      WHERE
        `phn` = '65484'
    ),
    'no',
    40395
  ),
  (
    1
    ,
    '63543',
    NULL,(
      SELECT
        `education` FROM `patients`
      WHERE
        `phn` = '63543'
    ),
    (
      SELECT
        `occupation` FROM `patients`
      WHERE
        `phn` = '63543'
    ),
    '547',
    '2021-02-14',
    'Kanthale',
    'no',
    'Agbopura',
    (
      SELECT
        YEAR(CURRENT_TIMESTAMP) - YEAR(`date_of_birth`)
      FROM
        `patients`
      WHERE
        `phn` = '63543'
    ),
    '2021-05-20',
    7,
    '2012-12-25',
    '2021-10-23',
    (SELECT
        `youngest_child_age`
      FROM
        `patients`
      WHERE `phn` = '63543'),
    'Degree',
    'Manager',
    '1985-06-14',
    'yes',
    (
      SELECT
        `height`
      FROM
        `patients`
      WHERE
        `phn` = '63543'
    ),
    (
      SELECT
        `weight`
      FROM
        `patients`
      WHERE
        `phn` = '63543'
    ),
    'no',
    87987
  ),
  (
    3
    ,
    '34264',
    NULL,(
      SELECT
        `education` FROM `patients`
      WHERE
        `phn` = '34264'
    ),
    (
      SELECT
        `occupation` FROM `patients`
      WHERE
        `phn` = '34264'
    ),
    '3213',
    '2021-04-10',
    'Kegalle',
    'no',
    'Kegalle',
    (
      SELECT
        YEAR(CURRENT_TIMESTAMP) - YEAR(`date_of_birth`)
      FROM
        `patients`
      WHERE
        `phn` = '34264'
    ),
    '2021-05-10',
    10,
    '2021-02-20',
    '2022-01-10',
    (
      SELECT
        `youngest_child_age`
      FROM
        `patients`
      WHERE
      `phn` = '34264'),
    'ol',
    'Security',
    '1982-10-19',
    'yes',
    (
      SELECT
        `height`
      FROM
        `patients`
      WHERE
        `phn` = '34264'
    ),
    (
      SELECT
        `weight`
      FROM
        `patients`
      WHERE
        `phn` = '34264'
    ),
    'no',
    23414
  ),
  (
    4
    ,
    '88453',
    NULL,(
      SELECT
        `education` FROM `patients`
      WHERE
        `phn` = '88453'
    ),
    (
      SELECT
        `occupation` FROM `patients`
      WHERE
        `phn` = '88453'
    ),
    '658',
    '2020-05-04',
    'Galle',
    'no',
    'Galle',
    (
      SELECT
        YEAR(CURRENT_TIMESTAMP) - YEAR(`date_of_birth`)
      FROM
        `patients`
      WHERE
        `phn` = '88453'
    ),
    '2021-05-25',
    7,
    '2021-03-28',
    '2022-01-30',
    (SELECT
        `youngest_child_age`
      FROM
        `patients`
      WHERE
        `phn` = '88453'),
    'ol',
    'Buinsessman',
    '1990-12-12',
    'yes',
    (
      SELECT
        `height`
      FROM
        `patients`
      WHERE
        `phn` = '88453'
    ),
    (
      SELECT
        `weight`
      FROM
        `patients`
      WHERE
        `phn` = '88453'
    ),
    'no',
    45755
  ),
  (
    3
    ,
    '55324',
    NULL,
    (
      SELECT
        `education` FROM `patients`
      WHERE
        `phn` = '55324'
    ),
    (
      SELECT
        `occupation` FROM `patients`
      WHERE
        `phn` = '55324'
    ),
    '6043',
    '2020-06-12',
    'Colombo',
    'no',
    'Paliyagoda',
    (
      SELECT
        YEAR(CURRENT_TIMESTAMP) - YEAR(`date_of_birth`)
      FROM
        `patients`
      WHERE
        `phn` = '55324'
    ),
    NULL,
    8,
    '2021-04-15',
    '2022-02-25',
    (SELECT
        `youngest_child_age`
      FROM
        `patients`
      WHERE
        `phn` = '55324'),
    'al',
    'Businessman',
    '1989-01-23',
    'yes',
    (
      SELECT
        `height`
      FROM
        `patients`
      WHERE
        `phn` = '55324'
    ),
    (
      SELECT
        `weight`
      FROM
        `patients`
      WHERE
        `phn` = '55324'
    ),
    'no',
    77834
  ),
  (
    2
    ,
    '98241',
    NULL,(
      SELECT
        `education` FROM `patients`
      WHERE
        `phn` = '98241'
    ),
    (
      SELECT
        `occupation` FROM `patients`
      WHERE
        `phn` = '98241'
    ),
    '542',
    '2020-05-14',
    'Gampaha',
    'no',
    'Kadawatha',
    (
      SELECT
        YEAR(CURRENT_TIMESTAMP) - YEAR(`date_of_birth`)
      FROM
        `patients`
      WHERE
        `phn` = '98241'
    ),
    '2021-06-17',
    10,
    '2021-03-14',
    '2022-02-01',
    (SELECT
        `youngest_child_age`
      FROM
        `patients`
      WHERE
        `phn` = '98241'),
    'degree',
    'Medical officer',
    '1989-01-23',
    'yes',
    (
      SELECT
        `height`
      FROM
        `patients`
      WHERE
        `phn` = '98241'
    ),
    (
      SELECT
        `weight`
      FROM
        `patients`
      WHERE
        `phn` = '98241'
    ),
    'no',
    59402
  ),
  (
    1
    ,
    '59698',
    NULL,(
      SELECT
        `education` FROM `patients`
      WHERE
        `phn` = '59698'
    ),
    (
      SELECT
        `occupation` FROM `patients`
      WHERE
        `phn` = '59698'
    ),
    '221',
    '2020-03-31',
    'Kaluthara',
    'no',
    'Moratuwa',
    (
      SELECT
        YEAR(CURRENT_TIMESTAMP) - YEAR(`date_of_birth`)
      FROM
        `patients`
      WHERE
        `phn` = '59698'
    ),
    '2021-03-15',
    10,
    '2021-01-18',
    '2022-01-30',
    (SELECT
        `youngest_child_age`
      FROM
        `patients`
      WHERE
        `phn` = '59698'),
    'al',
    'Buinsessman',
    '1978-11-11',
    'yes',
    (
      SELECT
        `height`
      FROM
        `patients`
      WHERE
        `phn` = '59698'
    ),
    (
      SELECT
        `weight`
      FROM
        `patients`
      WHERE
        `phn` = '59698'
    ),
    'yes',
    53442
  ),
  (
    2
    ,
    '87542',
    NULL,(
      SELECT
        `education` FROM `patients`
      WHERE
        `phn` = '87542'
    ),
    (
      SELECT
        `occupation` FROM `patients`
      WHERE
        `phn` = '87542'
    ),
    '9685',
    '2020-04-30',
    'Gampaha',
    'no',
    'Veyangoda',
    (
      SELECT
        YEAR(CURRENT_TIMESTAMP) - YEAR(`date_of_birth`)
      FROM
        `patients`
      WHERE
        `phn` = '87542'
    ),
    '2021-04-25',
    12,
    '2021-02-15',
    '2012-12-30',
    (SELECT
        `youngest_child_age`
      FROM
        `patients`
      WHERE
        `phn` = '87542'),
    'Degree',
    'Buinsessman',
    '1990-12-12',
    'yes',
    (
      SELECT
        `height`
      FROM
        `patients`
      WHERE
        `phn` = '87542'
    ),
    (
      SELECT
        `weight`
      FROM
        `patients`
      WHERE
        `phn` = '87542'
    ),
    'no',
    32412
  ),
  (
    2
    ,
    '35789',
    NULL,(
      SELECT
        `education` FROM `patients`
      WHERE
        `phn` = '35789'
    ),
    (
      SELECT
        `occupation` FROM `patients`
      WHERE
        `phn` = '35789'
    ),
    '432',
    '2020-06-04',
    'Colombo',
    'no',
    'Nugegoda',
    (
      SELECT
        YEAR(CURRENT_TIMESTAMP) - YEAR(`date_of_birth`)
      FROM
        `patients`
      WHERE
        `phn` = '35789'
    ),
    NULL,
    5,
    '2021-04-20',
    '2022-02-20',
    (SELECT
        `youngest_child_age`
      FROM
        `patients`
      WHERE
        `phn` = '35789'),
    'al',
    'Banker',
    '1990-04-22',
    'yes',
    (
      SELECT
        `height`
      FROM
        `patients`
      WHERE
        `phn` = '35789'
    ),
    (
      SELECT
        `weight`
      FROM
        `patients`
      WHERE
        `phn` = '35789'
    ),
    'yes',
    32423
  ),
  (
    1
    ,
    '65248',
    NULL,(
      SELECT
        `education` FROM `patients`
      WHERE
        `phn` = '65248'
    ),
    (
      SELECT
        `occupation` FROM `patients`
      WHERE
        `phn` = '65248'
    ),
    '432',
    '2021-02-04',
    'Kadawatha',
    'no',
    'Gampaha',
    (
      SELECT
        YEAR(CURRENT_TIMESTAMP) - YEAR(`date_of_birth`)
      FROM
        `patients`
      WHERE
        `phn` = '65248'
    ),
    '2021-02-10',
    10,
    '2020-11-28',
    '2021-08-29',
    (SELECT
        `youngest_child_age`
      FROM
        `patients`
      WHERE
        `phn` = '65248'),
    'degree',
    'Bank Manager',
    '1985-02-22',
    'yes',
    (
      SELECT
        `height`
      FROM
        `patients`
      WHERE
        `phn` = '65248'
    ),
    (
      SELECT
        `weight`
      FROM
        `patients`
      WHERE
        `phn` = '65248'
    ),
    'no',
    45755
  ),
  (
    1
    ,
    '45313',
    NULL,(
      SELECT
        `education` FROM `patients`
      WHERE
        `phn` = '45313'
    ),
    (
      SELECT
        `occupation` FROM `patients`
      WHERE
        `phn` = '45313'
    ),
    '98745',
    '2021-03-25',
    'Negombo',
    'no',
    'Katunayaka',
    (
      SELECT
        YEAR(CURRENT_TIMESTAMP) - YEAR(`date_of_birth`)
      FROM
        `patients`
      WHERE
        `phn` = '45313'
    ),
    '2021-04-10',
    12,
    '2021-01-01',
    '2021-10-25',
    (SELECT
        `youngest_child_age`
      FROM
        `patients`
      WHERE
        `phn` = '45313'),
    'al',
    'Buinsessman',
    '1991-01-15',
    'yes',
    (
      SELECT
        `height`
      FROM
        `patients`
      WHERE
        `phn` = '45313'
    ),
    (
      SELECT
        `weight`
      FROM
        `patients`
      WHERE
        `phn` = '45313'
    ),
    'no',
    40395
  ),
  (
    4
    ,
    '47564',
    NULL,(
      SELECT
        `education` FROM `patients`
      WHERE
        `phn` = '47564'
    ),
    (
      SELECT
        `occupation` FROM `patients`
      WHERE
        `phn` = '47564'
    ),
    '699',
    '2020-10-25',
    'Gampaha',
    'no',
    'Gampaha',
    (
      SELECT
        YEAR(CURRENT_TIMESTAMP) - YEAR(`date_of_birth`)
      FROM
        `patients`
      WHERE
        `phn` = '47564'
    ),
    '2020-12-12',
    12,
    '2020-08-08',
    '2021-06-02',
    (SELECT
        `youngest_child_age`
      FROM
        `patients`
      WHERE
        `phn` = '47564'),
    'degree',
    'Lawyer',
    '1990-12-07',
    'yes',
    (
      SELECT
        `height`
      FROM
        `patients`
      WHERE
        `phn` = '47564'
    ),
    (
      SELECT
        `weight`
      FROM
        `patients`
      WHERE
        `phn` = '47564'
    ),
    'yes',
    53442
  ),
  (
    3
    ,
    '87451',
    NULL,
    (
      SELECT
        `education` FROM `patients`
      WHERE
        `phn` = '87451'
    ),
    (
      SELECT
        `occupation` FROM `patients`
      WHERE
        `phn` = '87451'
    ),
    '521',
    '2020-10-28',
    'Colombo',
    'no',
    'Colombo7',
    (
      SELECT
        YEAR(CURRENT_TIMESTAMP) - YEAR(`date_of_birth`)
      FROM
        `patients`
      WHERE
        `phn` = '87451'
    ),
    '2021-11-10',
    7,
    '2020-09-30',
    '2021-06-30',
    (SELECT
        `youngest_child_age`
      FROM
        `patients`
      WHERE
        `phn` = '87451'),
    'degree',
    'Executive',
    '1986-09-18',
    'yes',
    (
      SELECT
        `height`
      FROM
        `patients`
      WHERE
        `phn` = '87451'
    ),
    (
      SELECT
        `weight`
      FROM
        `patients`
      WHERE
        `phn` = '87451'
    ),
    'no',
    43234
  ),
  (
    1
    ,
    '57985',
    NULL,(
      SELECT
        `education` FROM `patients`
      WHERE
        `phn` = '57985'
    ),
    (
      SELECT
        `occupation` FROM `patients`
      WHERE
        `phn` = '57985'
    ),
    '6656',
    '2021-01-10',
    'Colombo',
    'no',
    'Malabe',
    (
      SELECT
        YEAR(CURRENT_TIMESTAMP) - YEAR(`date_of_birth`)
      FROM
        `patients`
      WHERE
        `phn` = '57985'
    ),
    '2021-02-01',
    10,
    '2020-10-28',
    '2021-07-01',
    (SELECT
        `youngest_child_age`
      FROM
        `patients`
      WHERE
        `phn` = '57985'),
    'ol',
    'Technician',
    '1993-01-12',
    'yes',
    (
      SELECT
        `height`
      FROM
        `patients`
      WHERE
        `phn` = '57985'
    ),
    (
      SELECT
        `weight`
      FROM
        `patients`
      WHERE
        `phn` = '57985'
    ),
    'no',
    34294
  ),
  (
    1
    ,
    '86453',
    NULL,(
      SELECT
        `education` FROM `patients`
      WHERE
        `phn` = '86453'
    ),
    (
      SELECT
        `occupation` FROM `patients`
      WHERE
        `phn` = '86453'
    ),
    '569',
    '2020-12-14',
    'Gampaha',
    'no',
    'Kadawatha',
    (
      SELECT
        YEAR(CURRENT_TIMESTAMP) - YEAR(`date_of_birth`)
      FROM
        `patients`
      WHERE
        `phn` = '86453'
    ),
    '2021-01-25',
    9,
    '2020-10-28',
    '2021-07-30',
    (SELECT
        `youngest_child_age`
      FROM
        `patients`
      WHERE
        `phn` = '86453'),
    'degree',
    'Accountant',
    '1983-05-12',
    'yes',
    (
      SELECT
        `height`
      FROM
        `patients`
      WHERE
        `phn` = '86453'
    ),
    (
      SELECT
        `weight`
      FROM
        `patients`
      WHERE
        `phn` = '86453'
    ),
    'yes',
    79795
  );

INSERT INTO `completed_pregnancies`
  (`gravidity`, `sex`, `outcome`, `place_of_delivery`, `mode_of_delivery`,
  `antinatal_complication`, `phn`, `birth_weight`,
  `postnatal_complications`, `date_of_birth`, `date_of_miscarriage`)
VALUES
  (1, 'male', 'lb', 'BH Kantale', 'VD', NULL, '30502', 2.568, NULL, '2011-05-10', NULL),
  (1, 'female', 'lb', 'CSH Colombo', 'LSCS', 'PIH', '50643', 1.968, NULL, '2010-05-04', NULL),
  (2, 'male', 'lb', 'CSH Colombo', 'LSCS', 'PIH', '50643', 2.368, NULL, '2014-04-01', NULL),
  (1, 'female', 'lb', 'DGH Kegalle', 'VD', NULL, '65484', 2.098, NULL, '2017-01-10', NULL),
  (1, 'male', 'sb', 'BH Kantale', 'VD', 'GDM', '63543', 3.568, NULL, '2009-07-20', NULL),
  (2, 'male', 'lb', 'CSH Colombo', 'LSCS', 'GDM', '63543', 2.968, NULL, '2011-05-18', NULL),
  (3, 'female', 'lb', 'CSH Colombo', 'LSCS', 'GDM', '63543', 3.275, 'Neonatal jaundice', '2015-02-15', NULL),
  (1, 'male', 'lb', 'BH Kantale', 'VD', NULL, '34264', 1.876, 'Neonatal Jaundice', '2015-06-29', NULL),
  (2, 'female', 'lb', 'BH Kantale', 'LSCS', 'GDM', '34264', 3.268, 'Neonatal Jaundice', '2017-05-10', NULL),
  (1, 'male', 'lb', 'BH Karawanella', 'VD', NULL, '88453', 2.236, NULL, '2013-08-17', NULL),
  (2, 'male', 'lb', 'BH Karawanella', 'VD', NULL, '88453', 1.788, NULL, '2016-04-10', NULL),
  (3, 'male', 'lb', 'DGH Kegalle', 'VD', NULL, '88453', 2.356, NULL, '2019-12-10', NULL),
  (1, 'male', 'lb', 'CSH Colombo', 'LSCS', 'GDM', '55324', 4.068, 'Jaundice, dehydration, lactation failure', '2010-05-10', NULL),
  (2, 'female', 'lb', 'CSH Colombo', 'LSCS', NULL, '55324', 2.368, 'Sepsis', '2016-01-10', NULL),
  (1, 'male', 'lb', 'TH Ragama', 'VD', NULL, '98241', 2.788, NULL, '2017-03-19', NULL),
  (1, 'female', 'lb', 'TH Kandy', 'LSCS', 'GDM', '87542', 3.458, 'Neonatal jaundice', '2018-01-11', NULL),
  (1, 'male', 'lb', 'TH Kandy', 'VD', NULL, '35789', 1.986, NULL, '2018-07-30', NULL),
  (1, 'male', 'mc', 'BH Kantale', 'VD', NULL, '47564', 2.568, NULL, NULL, '2007-02-10'),
  (2, 'male', 'lb', 'BH Kantale', 'LSCS', 'GDM', '47564', 2.845, NULL, '2010-11-13', NULL),
  (3, 'male', 'lb', 'BH Kantale', 'VD', NULL, '47564', 1.698, NULL, '2016-08-13', NULL),
  (1, 'female', 'sb', 'DGH Trincomalee', 'VD', 'PIH', '87451', 2.000, NULL, '2012-08-13', NULL),
  (2, 'male', 'lb', 'BH Kantale', 'LSCS', 'PIH', '87451', 1.568, 'Preterm', '2015-07-13', NULL);

INSERT INTO
  `sessions`
  (
    `phn`,
    `gravidity`,
    `poa`,
    `slmc_id`,
    `clinic_number`,
    `appintment_number`,
    `session_time`
  )
VALUES
  ('50643',3,12,36213,'12','30','2021-01-31 09:12:00'),
  ('98241',2,24,33452,'13','43','2021-02-11 10:12:00'),
  ('35789',2,28,28454,'13','01','2021-03-19 12:12:00'),
  ('87451',3,21,33878,'11','20','2021-05-11 09:24:00'),
  ('47564',4,20,18934,'12','25','2021-06-23 10:35:00'),
  ('88453',4,15,32304,'13','23','2021-03-18 10:12:00'),
  ('74512',1,16,37541,'13','54','2021-04-29 11:34:00'),
  ('86453',1,18,32304,'11','12','2021-02-28 12:55:00'),
  ('59698',1,21,36213,'11','08','2021-01-30 11:23:00'),
  ('45313',1,28,21294,'13','09','2021-03-27 10:18:00'),
  ('87542',2,30,18934,'13','34','2021-04-26 10:22:00'),
  ('65248',1,17,36200,'13','12','2021-05-23 10:52:00'),
  ('47564',4,18,28994,'12','43','2021-06-26 08:32:00'),
  ('74512',1,27,33452,'12','20','2021-03-15 08:37:00'),
  ('98241',2,29,25493,'11','28','2021-01-14 09:56:00'),
  ('59698',1,21,23543,'11','27','2021-02-13 08:34:00'),
  ('87542',2,34,25043,'13','31','2021-03-21 08:12:00'),
  ('30403',1,33,28454,'13','19','2021-05-12 08:34:00'),
  ('55324',3,35,25493,'13','22','2021-03-17 07:46:00'),
  ('59698',1,36,35645,'12','17','2021-02-26 10:43:00');

INSERT INTO `prescriptions`
  (
    `session_id`,
    `issued_at`,
    `phn`,
    `gravidity`,
    `management`,
    `slmc_id`
  ) VALUES
  (1, '2021-01-31 09:20:00',(SELECT `phn` FROM `sessions` WHERE `session_id` = 1), (SELECT `gravidity` FROM `sessions` WHERE `session_id` = 1), 'RV in one month', (SELECT `slmc_id` FROM `sessions` WHERE `session_id` = 1)),
  (2,'2021-02-11 10:20:00', (SELECT `phn` FROM `sessions` WHERE `session_id` = 2), (SELECT `gravidity` FROM `sessions` WHERE `session_id` = 2), 'RV with TSH in one month', (SELECT `slmc_id` FROM `sessions` WHERE `session_id` = 2)),
  (3,'2021-03-19 12:20:00', (SELECT `phn` FROM `sessions` WHERE `session_id` = 3), (SELECT `gravidity` FROM `sessions` WHERE `session_id` = 3), 'RV with PPBS', (SELECT `slmc_id` FROM `sessions` WHERE `session_id` = 3)),
  (4,'2021-01-31 09:30:00', (SELECT `phn` FROM `sessions` WHERE `session_id` = 4), (SELECT `gravidity` FROM `sessions` WHERE `session_id` = 4), 'RV in 1 month for USS', (SELECT `slmc_id` FROM `sessions` WHERE `session_id` = 4)),
  (5,'2021-01-31 10:45:00', (SELECT `phn` FROM `sessions` WHERE `session_id` = 5), (SELECT `gravidity` FROM `sessions` WHERE `session_id` = 5), 'RV with PPBS', (SELECT `slmc_id` FROM `sessions` WHERE `session_id` = 5)),
  (6,'2021-01-31 10:20:00', (SELECT `phn` FROM `sessions` WHERE `session_id` = 6), (SELECT `gravidity` FROM `sessions` WHERE `session_id` = 6), 'RV for anormaly scan', (SELECT `slmc_id` FROM `sessions` WHERE `session_id` = 6)),
  (7,'2021-01-31 11:50:00', (SELECT `phn` FROM `sessions` WHERE `session_id` = 7), (SELECT `gravidity` FROM `sessions` WHERE `session_id` = 7), 'PCM 1g 6 Hourly for 3 days, RV in 2 weeks', (SELECT `slmc_id` FROM `sessions` WHERE `session_id` = 7)),
  (8,'2021-01-31 13:10:00', (SELECT `phn` FROM `sessions` WHERE `session_id` = 8), (SELECT `gravidity` FROM `sessions` WHERE `session_id` = 8), 'Omeprazole 20mg BD for 2 weeks, RV in 2 weeks', (SELECT `slmc_id` FROM `sessions` WHERE `session_id` = 8)),
  (9,'2021-01-31 11:40:00', (SELECT `phn` FROM `sessions` WHERE `session_id` = 9), (SELECT `gravidity` FROM `sessions` WHERE `session_id` = 9), 'Review in 2 months for uss', (SELECT `slmc_id` FROM `sessions` WHERE `session_id` = 9)),
  (10,'2021-01-31 10:40:00', (SELECT `phn` FROM `sessions` WHERE `session_id` = 10), (SELECT `gravidity` FROM `sessions` WHERE `session_id` = 10), 'UFR review with UFR report in 3 days', (SELECT `slmc_id` FROM `sessions` WHERE `session_id` = 10)),
  (11,'2021-01-31 10:30:00', (SELECT `phn` FROM `sessions` WHERE `session_id` = 11), (SELECT `gravidity` FROM `sessions` WHERE `session_id` = 11), 'USS scan of the thyroid, review with USS report', (SELECT `slmc_id` FROM `sessions` WHERE `session_id` = 11)),
  (12,'2021-01-31 11:20:00', (SELECT `phn` FROM `sessions` WHERE `session_id` = 12), (SELECT `gravidity` FROM `sessions` WHERE `session_id` = 12), 'RV in 1 month for PPBS report', (SELECT `slmc_id` FROM `sessions` WHERE `session_id` = 12)),
  (13,'2021-01-31 09:00:00', (SELECT `phn` FROM `sessions` WHERE `session_id` = 13), (SELECT `gravidity` FROM `sessions` WHERE `session_id` = 13), 'Review in 1 month for USS', (SELECT `slmc_id` FROM `sessions` WHERE `session_id` = 13)),
  (14,'2021-01-31 09:10:00', (SELECT `phn` FROM `sessions` WHERE `session_id` = 14), (SELECT `gravidity` FROM `sessions` WHERE `session_id` = 14), 'Plan for a LSCS due to risk factors', (SELECT `slmc_id` FROM `sessions` WHERE `session_id` = 14)),
  (15,'2021-01-31 10:20:00', (SELECT `phn` FROM `sessions` WHERE `session_id` = 15), (SELECT `gravidity` FROM `sessions` WHERE `session_id` = 15), 'RV in one month for routine review', (SELECT `slmc_id` FROM `sessions` WHERE `session_id` = 15)),
  (16,'2021-01-31 09:10:00', (SELECT `phn` FROM `sessions` WHERE `session_id` = 16), (SELECT `gravidity` FROM `sessions` WHERE `session_id` = 16), 'Blood picture and FBC and review with the reports', (SELECT `slmc_id` FROM `sessions` WHERE `session_id` = 16)),
  (17,'2021-01-31 08:40:00', (SELECT `phn` FROM `sessions` WHERE `session_id` = 17), (SELECT `gravidity` FROM `sessions` WHERE `session_id` = 17), 'PCM 1g 6h and Omeprazone 20mg bd for 1 week, reassess in 2 weeks', (SELECT `slmc_id` FROM `sessions` WHERE `session_id` = 17)),
  (18,'2021-01-31 08:50:00', (SELECT `phn` FROM `sessions` WHERE `session_id` = 18), (SELECT `gravidity` FROM `sessions` WHERE `session_id` = 18), 'RV for anormaly scan', (SELECT `slmc_id` FROM `sessions` WHERE `session_id` = 18)),
  (19,'2021-01-31 08:30:00', (SELECT `phn` FROM `sessions` WHERE `session_id` = 19), (SELECT `gravidity` FROM `sessions` WHERE `session_id` = 19), 'RV for dating scan', (SELECT `slmc_id` FROM `sessions` WHERE `session_id` = 19)),
  (20,'2021-01-31 11:10:00', (SELECT `phn` FROM `sessions` WHERE `session_id` = 20), (SELECT `gravidity` FROM `sessions` WHERE `session_id` = 20), 'Plan for NVD', (SELECT `slmc_id` FROM `sessions` WHERE `session_id` = 20));